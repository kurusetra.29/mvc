<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form(){
        return view('form');
    }

    // public function welcome(){
    //     return view('welcome');
    // }

    public function welcome_post(request $request){
        //dd ($request->all());
        $namaDepan = $request["namaDepan"];
        $namaBelakang = $request["namaBelakang"];
        // return "$namaDepan $namaBelakang ";
        return view('welcome');
    }
}
