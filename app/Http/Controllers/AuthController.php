<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function form(){
        return view('register');
    }
    public function welcome(Request $data){
        $Depan = $data ->namaDepan;
        $Belakang = $data ->namaBelakang;
        return view('welcome', compact('Depan','Belakang'));
    }
}
