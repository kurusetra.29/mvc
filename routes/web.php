<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

route::get('/', 'HomeController@index');

route::get('/register', 'AuthController@form');
route::post ('/welcome', 'AuthController@welcome');
// Route::get('/welcome', function () {
//     return view('welcome');
// });

// route::get('/form', 'RegisterController@form');

// route::get('/welcome', 'RegisterController@welcome');

// route::post('/welcome', 'RegisterController@welcome_post');